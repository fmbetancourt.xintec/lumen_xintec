<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class ProductoController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Nombre API: Listar Productos
     * Descripción: Retorna Lista de Productos
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $listProducto = Producto::all();
        return $this->successResponse($listProducto);
    }

    /**
     * Nombre API: Crear Producto
     * Descripción: Crea una instancia de Producto y lo almacena en BD
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request){
        $rules = [
            'nombre' => 'required|max:255',
            'marca' => 'required|max:255',
            'desc' => 'required|max:255',
            'precio' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $objProducto = Producto::create($request->all());

        return $this->successResponse($objProducto, Response::HTTP_CREATED);
    }

    /**
     * Nombre API: Obtener Producto
     * Descripción: Retorna instancia de Producto con id = $id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        $objProducto = Producto::findOrFail($id);
        return $this->successResponse($objProducto);
    }

    /**
     * Nombre API: Actualizar Producto
     * Descripción: Actualiza data de Producto con id = $id
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id){
        $rules = [
            'nombre' => 'max:255',
            'marca' => 'max:255',
            'desc' => 'max:255',
            'precio' => 'integer',
        ];

        $this->validate($request, $rules);

        $objProducto =Producto::findOrFail($id);

        $objProducto->fill($request->all());

        if ($objProducto->isClean()){
            return $this->errorResponse('Debe cambiar al menos un valor', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $objProducto->save();

        return $this->successResponse($objProducto,Response::HTTP_CREATED);
    }

    /**
     * Nombre API: Eliminar Producto
     * Descripción: Elimina Producto con id = $id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $objProducto = Producto::findOrFail($id);
        $objProducto->delete();
        return $this->successResponse($objProducto);
    }

}


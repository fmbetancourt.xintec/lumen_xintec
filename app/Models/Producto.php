<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model{

    use SoftDeletes;

    /**
     * Indica los campos que seran asignados en masa
     * @var string[]
     */
    protected $fillable = [
        'nombre',
        'marca',
        'desc',
        'precio',
    ];
}

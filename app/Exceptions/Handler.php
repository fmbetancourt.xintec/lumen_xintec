<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {

        if ($exception instanceof HttpException){
            $httpCode = $exception->getStatusCode();
            $message = Response::$statusTexts[$httpCode];

            return $this->errorResponse($message, $httpCode);
        }

        if ($exception instanceof ModelNotFoundException){
            $model = strtolower(class_basename($exception->getModel()));
            $httpCode = Response::HTTP_NOT_FOUND;
            $message = "No existe instancia de la clase {$model} con el ID indicado";

            return $this->errorResponse($message, $httpCode);
        }

        if ($exception instanceof AuthorizationException){
            $message = $exception->getMessage();
            $httpCode = Response::HTTP_FORBIDDEN;

            return $this->errorResponse($message, $httpCode);
        }

        if ($exception instanceof AuthenticationException){
            $message = $exception->getMessage();
            $httpCode = Response::HTTP_UNAUTHORIZED;

            return $this->errorResponse($message, $httpCode);
        }

        if ($exception instanceof ValidationException){
            $message = $exception->validator->errors()->getMessages();
            $httpCode = Response::HTTP_UNPROCESSABLE_ENTITY;

            return $this->errorResponse((string)$message, $httpCode);
        }

        return parent::render($request, $exception);
    }
}

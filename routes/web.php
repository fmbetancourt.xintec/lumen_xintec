<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

# Listar Productos
$router->get('/productos', 'ProductoController@index');

# Crear Producto
$router->post('/productos', 'ProductoController@store');

# Obtener Producto
$router->get('/productos/{id}', 'ProductoController@show');

# Actualizar Producto
$router->put(  '/productos/{id}', 'ProductoController@update');
$router->patch('/productos/{id}', 'ProductoController@update');

# Eliminar Producto
$router->delete('/productos/{id}', 'ProductoController@destroy');

